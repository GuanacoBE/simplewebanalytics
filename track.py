import json
import boto3
from datetime import date
from referer_parser import Referer

client = boto3.client('dynamodb')

def incrementViews(day, page, source, country):
    update_expression = ["ADD #views :increment"]
    if source is not None:
        update_expression.append(f", source_{source} :increment")
    if country is not None:
        update_expression.append(f", country_{country} :increment")

    client.update_item(
        TableName='SiteStatistics',
        Key={
            'day': {'S': day},
            'page': {'S': page}
        },
        UpdateExpression=''.join(update_expression),
        ExpressionAttributeNames={
            '#views': 'views'
        },
        ExpressionAttributeValues={
            ':increment': {'N': '1'}
        },
        ReturnValues='ALL_NEW'
    )


def invoke(event, context):
    data = json.loads(event['body'])

    source = 'Direct'

    if ("source" in data and data['source']):
        referer = Referer(data['source']).referer
        source = referer if referer is not None else 'Unknown'

    today = date.today().strftime("%Y-%m-%d")

    incrementViews(today, data['page'], source, data['country'])

    response = {
        "statusCode": 200
    }

    return response
