# SimpleWebAnalytics
Simple Web Analytics for website. This will creates the following resources into your account:
* Two lambda functions:
  * One lambda that persists the viewer information into a Dynamo table
  * One lambda that fetches the information from the Dynamo table and returns an html page with charts.
* A DynamoDB table `SiteStatistics`


## How to deploy ?
```
serverless deploy
```
