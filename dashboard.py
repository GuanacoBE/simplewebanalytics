import boto3
import datetime
import random

client = boto3.client('dynamodb')

def get_days(n = 7):
    today = datetime.datetime.now()
    days = [today - datetime.timedelta(days=i) for i in range(n)]
    result = list(map(lambda x: x.date().strftime("%Y-%m-%d"), days))
    return result[::-1]

def fetchData(number_days = 7):
    days = get_days(number_days)

    result = []

    for day in days:
        response = client.query(
            TableName='SiteStatistics',
            KeyConditionExpression='#day = :day',
            ExpressionAttributeNames={
                '#day': 'day'
            },
            ExpressionAttributeValues={
                ':day': {'S': day}
            }
        )

        if (len(response['Items']) > 0):
            result.extend(response['Items'])
        else:
            result.append({'views': {'N': '0'}, 'day': {'S': day}})
            
    return result


def build_charts_data(dynamo_items):
    country_result = {
        "labels": [],
        "data": []
    }

    day_result = {
        "labels": [],
        "data": []
    }

    source_result = {
        "labels": [],
        "data": []
    }

    page_result = {
        "labels": [],
        "data": []
    }

    count_per_country = {}
    count_per_day = {}
    count_per_source = {}
    count_per_page = {}

    # [{'country_IE': {'N': '1'}, 'views': {'N': '1'}, 'day': {'S': '2022-04-18'}, 'source_Direct': {'N': '1'}, 'page': {'S': 'Home'}}]

    for item in dynamo_items:
        day = item["day"]["S"]
        views = int(item["views"]["N"])
        count_per_day[day] = count_per_day.get(day, 0) + views

        if (views > 0):
            count_per_page[item["page"]["S"]] = count_per_page.get(item["page"]["S"], 0) + views
            for key, value in item.items():
                if key.startswith('country_'):
                    country = key[8:]
                    count_per_country[country] = count_per_country.get(country, 0) + int(value["N"])
                elif key.startswith('source_'):
                    source = key[7:]
                    count_per_source[source] = count_per_source.get(source, 0) + int(value["N"])

    country_result["labels"] = list(count_per_country.keys())
    country_result["data"] = list(count_per_country.values())

    day_result["labels"] = list(count_per_day.keys())
    day_result["data"] = list(count_per_day.values())

    source_result["labels"] = list(count_per_source.keys())
    source_result["data"] = list(count_per_source.values())

    page_result["labels"] = list(count_per_page.keys())
    page_result["data"] = list(count_per_page.values())

    result = {}
    result["country"] = country_result
    result["day"] = day_result
    result["source"] = source_result
    result["page"] = page_result

    return result


def invoke(event, context):
    dynamo_items = fetchData(int(event['queryStringParameters']['days']) if event['queryStringParameters']['days'] else None)
    chart_data = build_charts_data(dynamo_items)

    response = {
        "statusCode": 200,
        "headers": {
            "Content-Type": "text/html"
        },
        "body": build_html(chart_data)
    }

    return response



def build_html(chart_data):
    max_labels = max(len(chart_data['country']['labels']), len(chart_data['day']['labels']), len(chart_data['source']['labels']), len(chart_data['source']['labels']))
    random_colors = ["'#%06x'" % random.randint(0, 0xFFFFFF) for i in range(max_labels)]
    return f"""
        const countryChartCtx = document.getElementById('countryChart');
        new Chart(countryChartCtx, {{
            type: 'pie',
            data: {{
                labels: {chart_data['country']['labels']},
                datasets: [{{
                    data: {chart_data['country']['data']},
                    backgroundColor: [{', '.join(random_colors[:len(chart_data['country']['labels'])])}]
                }}]
            }}
        }});

        const perDayCtx = document.getElementById('perDay');
        new Chart(perDayCtx, {{
            type: 'line',
            data: {{
                labels: {chart_data['day']['labels']},
                datasets: [{{
                    label: 'Visitors per day',
                    data: {chart_data['day']['data']},
                    fill: false,
                    tension: 0.1,
                    borderColor: {random_colors[0]}
                }}]
            }},
            options: {{
                scales: {{
                    y: {{
                        beginAtZero: true
                    }}
                }}
            }}
        }});

        const perSourceCtx = document.getElementById('perSource');
        new Chart(perSourceCtx, {{
            type: 'pie',
            data: {{
                labels: {chart_data['source']['labels']},
                datasets: [{{
                    data: {chart_data['source']['data']},
                    backgroundColor: [{', '.join(random_colors[:len(chart_data['source']['labels'])])}]
                }}]
            }}
        }});

        const perPageCtx = document.getElementById('perPage');
        new Chart(perPageCtx, {{
            type: 'bar',
            data: {{
                labels: {chart_data['page']['labels']},
                datasets: [{{
                    data: {chart_data['page']['data']},
                    label: "Count per page",
                    backgroundColor: [{', '.join(random_colors[:len(chart_data['page']['labels'])])}]
                }}]
            }},
            options: {{
                indexAxis: 'y',
                plugins: {{
                    legend: {{
                        display: false
                    }}
                }}
            }}
        }});"""
